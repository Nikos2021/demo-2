<?php

namespace App\Http\Controllers;
use Auth;
use App\Models\UserExtra;
use Illuminate\Http\Request;
use Redirect;
use Inertia\Inertia;
class ShopifyController extends Controller
{
    public function index(){
        $user_extra = Auth::user()->extras;
        return Inertia::render('Shopify',[
            'shopify' => $user_extra
        ]);
    }

    // Oauth Request
    public function oAuthRequest() {
        $user_extra = Auth::user()->extras;
        if( $user_extra &&  $user_extra->shop_url) {
            $client_id=env('SHOPIFY_CLIENT_ID','29a2a524c5b955c249a6741e4c6d0164');
            $scope=env('SHOPIFY_SCOPE','read_products,write_products,read_product_listings,write_product_listings');
            $redirect_uri=env('SHOPIFY_REDIRECT_URI','http://127.0.0.1:8000/oAuthCodeVerification');
            $state='nonce01';
            $shop_url=$user_extra->shop_url;
            $url= $shop_url.'/admin/oauth/authorize?client_id='.$client_id.'&scope='.$scope.'&redirect_uri='.$redirect_uri.'&state='.$state.'&grant_options[]=';
            return Redirect::to($url);
        }else {
            return response()->json(['status'=>'error','message'=>'Invalid Request,No Store URL Found'],422);
        }
        
    }
    //Get OAuth access code
    public function oAuthCodeVerification(Request $request){
        $user_extra = Auth::user()->extras;
        if ($request->code){
            $serverResponse=$this->getAccessToken($request->code);
            $serverResponse=json_decode($serverResponse);
            if($serverResponse->access_token){
                $user_extra->shopify_token=$serverResponse->access_token;
                $user_extra->save();
                return Redirect::route('shopify')->with(['message', 'Connected Successfully']);
               
            }else {
                return Redirect::route('shopify')->withErrors(['message', 'Invalid Response']);
                
            }  
        }else {
            return Redirect::route('shopify')->withErrors(['message', 'No Authorization Code found']);
        }

    }
    // Get AccessTOken from Access Code
    public function getAccessToken($code){
        $user_extra = Auth::user()->extras;
        $shop_url=$user_extra->shop_url;
        $client = new \GuzzleHttp\Client();
        $endpoint=$shop_url.'/admin/oauth/access_token';

        $response = $client->request('POST', $endpoint, ['query' => [
            'client_id' => env('SHOPIFY_CLIENT_ID','29a2a524c5b955c249a6741e4c6d0164'), 
            'client_secret' => env('SHOPIFY_CLIENT_SECRET','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'),
            'code' => $code,
        ]]);
        $statusCode = $response->getStatusCode();
        $content = $response->getBody();
        return  $content;
    }

    public function disconnectStore(){
        $user_extra = Auth::user()->extras;
        if ($user_extra) {
            $user_extra->delete();
            return Redirect::route('shopify')->with('message','Store Disconnected Successfully');
        }else{
            return Redirect::route('shopify')->with('message','No Store Found');
        }
    }

    public function saveStoreUrl(Request $request) {
        if ($request->shop_url) {
            $user_extra = Auth::user()->extras;
            if ($user_extra) {
                $user_extra->user_id=Auth::user()->id;
                $user_extra->shop_url=$request->shop_url;
                $user_extra->save();
                // return Inertia::render('Shopify',[
                //     'shopify' => $user_extra,
                //     'message' => 'Store URL Saved Successfully'
                // ]);
                return Redirect::route('shopify')->with('success','Store URL Saved Successfully');
               
            }else {
                $user_extra = new UserExtra();
                $user_extra->user_id=Auth::user()->id;
                $user_extra->shop_url=$request->shop_url;
                $user_extra->save();
                // return Inertia::render('Shopify',[
                //     'shopify' => $user_extra,
                //     'message' => 'Store URL Saved Successfully'
                // ]);
                return Redirect::route('shopify')->with('success','Store URL Saved Successfully');
            }
            }
    }
}
