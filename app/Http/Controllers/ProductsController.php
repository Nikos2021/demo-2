<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Response;
use Inertia\Inertia;
class ProductsController extends Controller
{
    public function index(){
        
        if ($this->getShopifyProducts()) {
            $shopifyProductsResponseStatus=$this->getShopifyProducts()->getStatusCode();
        if ($shopifyProductsResponseStatus==201) {
            $shopifyProducts=json_decode($this->getShopifyProducts()->getContent())->data;
            $shopifyProducts=(array)  $shopifyProducts ? $shopifyProducts->products : [];
            return Inertia::render('Products',[
                'Products' => $shopifyProducts
            ]);
        }else {
           
            return Inertia::render('Products',[
                'Products' => []
                
            ]);
        }   
    }else {
        return Inertia::render('Products',[
            'Products' => []
            
        ]);
    }
        }
        

    public function getShopifyProducts(){
        $shopify = Auth::user()->extras;;
        if ($shopify) {
            if($shopify->shopify_token) {
            $shop_url=$shopify->shop_url;
            $access_token=$shopify->shopify_token;
            $client = new \GuzzleHttp\Client();
            $endpoint=$shop_url.'/admin/api/2021-04/products.json';
            $headers = [
                'Content-Type'        => 'application/json',
                'X-Shopify-Access-Token' => $access_token,        
            ];
            try {
                $response = $client->request('GET', $endpoint, ['headers' => $headers]);
                $statusCode = $response->getStatusCode();
                if($statusCode=200) {
                    $content = $response->getBody();
                    // return json_decode($content);
                    
                    return Response::json([
                        'data' => json_decode($content)
                    ], 201);
                }
                    
                
                
                
                } catch (\GuzzleHttp\Exception\ClientException $th) {return Response::json([
                    'error' => $th->getMessage()
                ], 401);}
            } 
        }
    }
}
