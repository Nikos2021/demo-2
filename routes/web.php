<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');

    Route::get('products', [App\Http\Controllers\ProductsController::class, 'index'])->name('products');
    //---Shopify Routes
    Route::get('shopify', [App\Http\Controllers\ShopifyController::class, 'index'])->name('shopify');
    Route::post('saveStoreUrl', [App\Http\Controllers\ShopifyController::class, 'saveStoreUrl'])->name('saveStoreUrl');
    Route::get('disconnectStore', [App\Http\Controllers\ShopifyController::class, 'disconnectStore'])->name('disconnectStore');
    Route::get('oAuthRequest', [App\Http\Controllers\ShopifyController::class, 'oAuthRequest'])->name('oAuthRequest');
    Route::get('oAuthCodeVerification', [App\Http\Controllers\ShopifyController::class, 'oAuthCodeVerification'])->name('oAuthCodeVerification');
    Route::get('getAccessToken', [App\Http\Controllers\ShopifyController::class, 'getAccessToken'])->name('getAccessToken');
});
    
